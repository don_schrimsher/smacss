# smacss

Scaffold out a sass directory structure using the SMACSS format.

[![build status](https://secure.travis-ci.org/sterlingw/smacss.png)](http://travis-ci.org/sterlingw/smacss)

## Installation

This module is installed via npm:

``` bash
$ npm install smacss
```

## Example Usage

Make sure you're in the right directory:
``` bash
$ mkdir scss
```

``` bash
$ cd scss
```

Run the command:
``` js
smacss
```
